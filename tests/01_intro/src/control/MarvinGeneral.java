package control;

import java.awt.Color;

import marvin.gui.MarvinImagePanel;
import marvin.image.MarvinImage;
import marvin.image.MarvinImageMask;
import marvin.io.MarvinImageIO;
import marvin.plugin.MarvinImagePlugin;
import marvin.util.MarvinPluginLoader;
import marvin.util.MarvinAttributes;

public class MarvinGeneral {
	private MarvinImagePanel imagePanel;
	private MarvinImage image;
	private MarvinImagePlugin imagePlugin;
	
	public MarvinGeneral()
	{
		
	}
	
	
	public MarvinImagePanel seImageToPanel(String imagePath)
	{
		imagePanel = new MarvinImagePanel();
		image = MarvinImageIO.loadImage(imagePath);
		image.resize(550, 250);
		imagePanel.setImage(image);
		return imagePanel;
	}
	
	
	public void applyFilter(MarvinImagePanel panel, MarvinImage image, String filter)
	{
		MarvinImage alterImage = image;
		
		imagePlugin = MarvinPluginLoader.loadImagePlugin(filter);
		imagePlugin.process(alterImage, alterImage);
		panel.setImage(alterImage);
	}
	
	public void applyFilter(MarvinImagePanel panel, MarvinImage image, String filter, MarvinImageMask mask)
	{
		MarvinImage alterImage = image;
		
		imagePlugin = MarvinPluginLoader.loadImagePlugin(filter);
		imagePlugin.setAttribute("thereshold", 50000);
		imagePlugin.process(alterImage, alterImage, mask);
		panel.setImage(alterImage);
	}
	
	public void applyFilter(MarvinImagePanel panel, MarvinImage image, String filter, MarvinAttributes attr, MarvinImageMask mask)
	{
		MarvinImage alterImage = image;
		
		imagePlugin = MarvinPluginLoader.loadImagePlugin(filter);
		imagePlugin.load();
		imagePlugin.setAttribute("thereshold", 50000);
		imagePlugin.process(alterImage, alterImage, attr, mask, true);
		alterImage = this.showCorners(alterImage, attr, 6);
		panel.setImage(alterImage);
	}
	
	
	
	public MarvinImage showCorners(MarvinImage image, MarvinAttributes attr, int rectSize){
		MarvinImage ret = image.clone();
		int[][] cornernessMap = (int[][]) attr.get("cornernessMap");
		int rsize=0;
		for(int x=0; x<cornernessMap.length; x++){
			for(int y=0; y<cornernessMap[0].length; y++){
				// Is it a corner?
				if(cornernessMap[x][y] > 0){
					rsize = Math.min(Math.min(Math.min(x, rectSize), Math.min(cornernessMap.length-x, rectSize)), Math.min(Math.min(y, rectSize), Math.min(cornernessMap[0].length-y, rectSize)));
					ret.fillRect(x, y, rsize, rsize, Color.red);
				}				
			}
		}
		
		return ret;
	}
	
	
	
	public void resetImage(MarvinImagePanel panel)
	{
		panel.removeAll();
	}		
	
	public MarvinImagePanel newImagePanel()
	{
		this.imagePanel = new MarvinImagePanel();
		return this.imagePanel;
	}	
	public void setImagePanel(MarvinImagePanel panel)
	{
		this.imagePanel = panel;
	}	
	public MarvinImagePanel getImagePanel()
	{
		return this.imagePanel;
	}
	public MarvinImagePanel addImageToPanel(MarvinImagePanel panel, MarvinImage image)
	{
		panel.setImage(image);
		return panel;
	}
	
	public MarvinImage setImage(String image)
	{
		this.image = MarvinImageIO.loadImage(image);
		this.image.resize(550, 250);
		return this.image;
	}
	
	public MarvinImage getImage()
	{
		return this.image;
	}
}
