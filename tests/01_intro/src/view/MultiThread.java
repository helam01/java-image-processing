package view;


import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import sun.misc.PerformanceLogger;

import marvin.gui.MarvinImagePanel;
import marvin.image.MarvinImage;
import marvin.image.MarvinImageMask;
import marvin.io.MarvinImageIO;
import marvin.plugin.MarvinImagePlugin;
import marvin.thread.MarvinThread;
import marvin.thread.MarvinThreadEvent;
import marvin.thread.MarvinThreadListener;
import marvin.util.MarvinPluginLoader;


public class MultiThread implements MarvinThreadListener{
	private JFrame mainFrame;
	private JButton btnSingleThread, btnMultiThread, btnReset;	
	private JLabel labelPerformance;	
	private MarvinImagePanel imagePanel;	
	private MarvinImage imageIn, imageOut, originalImage;	
	private int threadFinished;	
	private long processStarTime;
	
	public MultiThread()
	{
		mainFrame = new JFrame("::Multi Thread Sample::");
		mainFrame.setSize(800,600);
		mainFrame.setVisible(true);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		ButtonHandler btnHandler = new ButtonHandler();
		btnSingleThread = new JButton("Single Thread");
		btnMultiThread = new JButton("Multi Thread");
		btnReset = new JButton("Reset Image");
		
		btnSingleThread.addActionListener(btnHandler);
		btnMultiThread.addActionListener(btnHandler);
		btnReset.addActionListener(btnHandler);
		
		labelPerformance = new JLabel("Performance: ");
		
		JPanel panelIntern = new JPanel();
		panelIntern.add(btnSingleThread);
		panelIntern.add(btnMultiThread);
		panelIntern.add(btnReset);
		
		JPanel panelBottom = new JPanel();
		panelBottom.setLayout(new BorderLayout());
		panelBottom.add(panelIntern, BorderLayout.NORTH);
		panelBottom.add(labelPerformance, BorderLayout.SOUTH);
		
		imagePanel = new MarvinImagePanel();
		
		//Container
		Container con = mainFrame.getContentPane();
		con.setLayout(new BorderLayout());
		con.add(imagePanel, BorderLayout.NORTH);
		con.add(panelBottom, BorderLayout.SOUTH);
		
		//Load Image
		loadImages();		
	}
	
	
	private void loadImages()
	{
		originalImage = MarvinImageIO.loadImage("./images/image01.jpg");
		originalImage.resize(600, 350);
		imageIn = new MarvinImage(originalImage.getWidth(), originalImage.getHeight());
		imageOut = new MarvinImage(originalImage.getWidth(), originalImage.getHeight());;
		imagePanel.setImage(originalImage);
	}
	
	
	@Override
	public void threadFinished(MarvinThreadEvent e) {
		threadFinished ++;
		
		if ( threadFinished == 2 ) {
			imageOut.update();
			imagePanel.setImage(imageOut);
			labelPerformance.setText("Performance: " + (System.currentTimeMillis() - processStarTime) + " milliseconds (Multi Thread)");
			mainFrame.repaint();
		}
		
	}
	
	
	private void singleThread()
	{
		processStarTime = System.currentTimeMillis();
		MarvinImagePlugin plgImage = MarvinPluginLoader.loadImagePlugin("org.marvinproject.image.statistical.Maximum.jar");
		plgImage.process(imageIn, imageOut);
		imageOut.update();
		imagePanel.setImage(imageOut);
		labelPerformance.setText("Performance: " + (System.currentTimeMillis() - processStarTime)+ " milliseconds (single Thread)");
		mainFrame.repaint();
	}
	
	
	private void multiThread()
	{
		processStarTime = System.currentTimeMillis();
		
		//load Plug-ings
		MarvinImagePlugin plgImage1 = MarvinPluginLoader.loadImagePlugin("org.marvinproject.image.statistical.Maximum.jar");
		MarvinImagePlugin plgImage2 = MarvinPluginLoader.loadImagePlugin("org.marvinproject.image.statistical.Maximum.jar");
		
		//create masks
		MarvinImageMask mask1 = new MarvinImageMask(
			imageIn.getWidth(),		// width
			imageIn.getHeight(),	// height
			0, 						// x-start
			0, 						// y-start
			imageIn.getWidth(),		// region's width
			imageIn.getHeight() /2	// region's height
		);
		
		MarvinImageMask mask2 = new MarvinImageMask(
			imageIn.getWidth(), 	 // width
			imageIn.getHeight(),	 // height
			0,						 // x-start
			imageIn.getHeight() / 2, // y-start
			imageIn.getWidth(),		 // region's width
			imageIn.getHeight() / 2  // region's height
		);
		
		
		MarvinThread mvThread1 = new MarvinThread(plgImage1, imageIn, imageOut, mask1);
		MarvinThread mvThread2 = new MarvinThread(plgImage2, imageIn, imageOut, mask2);
		
		mvThread1.addThreadListener(this);
		mvThread2.addThreadListener(this);
		
		mvThread1.start();
		mvThread2.start();
		
		threadFinished = 0;
		
				
	}
	
	
	private class ButtonHandler implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			imageIn = originalImage.clone();
			
			if ( e.getSource() == btnSingleThread ) {
				singleThread();
			}
			
			if ( e.getSource() == btnMultiThread ) {
				multiThread();
			}
			
			if ( e.getSource() == btnReset ) {
				imagePanel.setImage(originalImage);
			}
			
		}
		
	}

}
























