package view;

import java.awt.BorderLayout; 
import java.awt.Color;
import java.awt.Container; 
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 

import javax.swing.JButton; 
import javax.swing.JFrame; 
import javax.swing.JPanel;

import marvin.gui.MarvinImagePanel;
import marvin.image.MarvinImage;
import marvin.io.MarvinImageIO;
import marvin.plugin.MarvinImagePlugin;
import marvin.util.MarvinPluginLoader;

import marvin.video.MarvinJavaCVAdapter;
import marvin.video.MarvinVideoInterface;

public class Test {
	private JFrame mainFrame;	
	private JPanel panelBottom;	
	private MarvinImagePanel imagePanel;
	private MarvinImage image, image2, backupImage;
	private MarvinImagePlugin imagePlugin;
	
	private MarvinVideoInterface 	videoInterface;
	private MarvinImagePanel 		videoPanel;
	
	
	public Test()
	{
		mainFrame = new JFrame("::My First Image Processing App::");
		mainFrame.setSize(800, 600);
		mainFrame.setVisible(true);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		imagePanel = new MarvinImagePanel();
		
		Container lc = mainFrame.getContentPane();
		lc.setLayout(new BorderLayout());
		lc.add(imagePanel, BorderLayout.NORTH);
		
		
		
		/*
		 * IMAGE TESTS
		 */
		image = new MarvinImage(200,200);
		image.fillRect(250, 10, 100, 100, Color.blue);
		
		image2 = MarvinImageIO.loadImage("./images/image01.jpg");
		MarvinImageIO.saveImage(image2, "./images/image01_test.jpg");
		image2.resize(350, 300);
		
		imagePlugin = MarvinPluginLoader.loadImagePlugin("org.marvinproject.image.artistic.mosaic.jar");
		imagePlugin.process(image2,image2);
		image2.update();
		
		imagePanel.setImage(image2);
		
		
		videoPanel = new MarvinImagePanel();
		videoInterface = new MarvinJavaCVAdapter();
		videoInterface.connect(0);
	}
}
