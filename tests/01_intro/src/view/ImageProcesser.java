package view;

import java.awt.BorderLayout; 
import java.awt.Container; 
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 

import javax.swing.JButton; 
import javax.swing.JFrame;
import javax.swing.JPanel;

import marvin.gui.MarvinImagePanel;
import marvin.image.MarvinImage;
import marvin.io.MarvinImageIO;
import marvin.image.MarvinImageMask; 
import marvin.plugin.MarvinImagePlugin; 
import marvin.util.MarvinPluginLoader;

public class ImageProcesser implements ActionListener{

	private JFrame mainFrame;
	
	private JPanel panelBottom;
	
	private JButton btnGray, btnEdgeDetector, btnInvert, btnReset;
	
	private MarvinImagePanel imagePanel;
	private MarvinImage image, backupImage;
	private MarvinImagePlugin imagePlugin;
	
	
	
	public ImageProcesser()
	{
		mainFrame = new JFrame("::My First Image Processing App::");
		mainFrame.setSize(420, 450);
		mainFrame.setVisible(true);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		btnGray = new JButton("Gray");
		btnGray.addActionListener(this);
		
		btnEdgeDetector = new JButton("Edge Detector");
		btnEdgeDetector.addActionListener(this);
		
		btnInvert = new JButton("Invert Colors");
		btnInvert.addActionListener(this);
		
		btnReset = new JButton("Reset");
		btnReset.addActionListener(this);
		
		panelBottom = new JPanel();
		panelBottom.add(btnGray);
		panelBottom.add(btnEdgeDetector);
		panelBottom.add(btnInvert);
		panelBottom.add(btnReset);
		
		imagePanel = new MarvinImagePanel();
				
		Container lc = mainFrame.getContentPane();
		lc.setLayout(new BorderLayout());
		lc.add(panelBottom, BorderLayout.SOUTH);
		lc.add(imagePanel, BorderLayout.NORTH);
		
		//Load Image
		image = MarvinImageIO.loadImage("./images/image01.jpg");
		image.resize(400, 350);
		backupImage = image.clone();
		imagePanel.setImage(image);
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		image = backupImage.clone();
		
		if ( e.getSource() == btnGray ) {
			imagePlugin = MarvinPluginLoader.loadImagePlugin("org.marvinproject.image.artistic.television.jar");
			imagePlugin.process(image, image);
		}
		
		if ( e.getSource() == btnEdgeDetector ) {
			imagePlugin = MarvinPluginLoader.loadImagePlugin("org.marvinproject.image.edge.edgeDetector.jar");
			imagePlugin.process(image, image);			
		}
		
		if ( e.getSource() == btnInvert ) {
			imagePlugin = MarvinPluginLoader.loadImagePlugin("org.marvinproject.image.color.invert.jar");
			imagePlugin.process(image, image);
		}
		
		if ( e.getSource() == btnReset ) {
			
		}
		
		image.update();
		imagePanel.setImage(image);
	}

}

