package templates;

import java.awt.Color;
import java.awt.Container; 
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 
import java.awt.image.BufferedImage;

import javax.swing.GroupLayout;
import javax.swing.JButton; 
import javax.swing.JFrame; 
import javax.swing.JPanel;
import javax.swing.JLabel;

import sun.rmi.runtime.Log;

import java.util.ArrayList;
import java.util.EventObject;

import control.MarvinGeneral;

import marvin.gui.MarvinImagePanel;
import marvin.image.MarvinImage;
import marvin.image.MarvinImageMask;
import marvin.io.MarvinImageIO;
import marvin.util.MarvinAttributes;

public class MainTemplate implements ActionListener{
	private JFrame mainFrame;
	private JPanel mainPanel, menuPanel, imagesPanel, image1Panel, image2Panel;
	private JLabel image1Title, image2Title;
	private Container container;
	private GroupLayout layout;
	
	private ArrayList<String> menuItens = new ArrayList<String>();
	private ArrayList<JButton> menu = new ArrayList<JButton>(); 
	
	private BufferedImage img1, img2;
	
	private MarvinGeneral marvin;	
	private MarvinImagePanel imagePanel, imagePanel2;
	private MarvinImage image, image2, imageBkp;
	private MarvinAttributes attr;
	private MarvinImageMask mask;
	
	public MainTemplate()
	{
		// Set the main Frame
		mainFrame = new JFrame(":: Image Processing Project ::");
		mainFrame.setSize(800, 700);
		mainFrame.setVisible(true);
		
		// Set the frame container
		container = mainFrame.getContentPane();
		
		// Instantiate new marvinGeneral object
		marvin = new MarvinGeneral();
		
		// Set the Panels
		mainPanel = new JPanel();
		imagesPanel = new JPanel();
		menuPanel = new JPanel();
		
		menuPanel.setBackground(Color.lightGray);
		menuPanel.setSize(180, 580);
		
		imagesPanel.setBackground(Color.white);
		imagesPanel.setSize(600, 580);
		
		image1Panel = new JPanel();
		image1Panel.setBackground(Color.lightGray);
		image1Panel.setSize(580, 260);
		image1Panel.add(marvin.seImageToPanel("./images/image01.jpg"));
		
		image2Panel = new JPanel();
		image2Panel.setBackground(Color.lightGray);
		image2Panel.setSize(580, 260);
		//image = marvin.setImage("./images/more/Mona_Lisa_headcrop.jpg");
		image = marvin.setImage("./images/image01.jpg");
		//image.setIntColor(10, 200, 150);
		imageBkp = image;
		imagePanel = marvin.newImagePanel();
		image2Panel.add(marvin.addImageToPanel(imagePanel, image));
		
		imagesPanel.add(new JLabel("Original Image: "));
		imagesPanel.add(image1Panel);
		imagesPanel.add(new JLabel("Altered Image: "));
		imagesPanel.add(image2Panel);
		
		
		// TESTES
		System.out.println( ""  );
		/*
		for (int i=0; i< imagearray.length; i++) {
			System.out.println(imagearray[i]);
		}*/
	
		// Set the buttons and add it in the menuPanel	
		
		menu = setMenu();				
		for (int i = 0; i < menu.size(); i++) {
			menuPanel.add(menu.get(i));
			menu.get(i).addActionListener(this);
		}
		
		// Set the layout
		layout = new GroupLayout(container);
		mainPanel.setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		layout.setHorizontalGroup(
				layout.createSequentialGroup()
					.addComponent(menuPanel, 180, 180, 180)
					.addComponent(imagesPanel)
		);
		
		layout.setVerticalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addComponent(menuPanel)
				.addComponent(imagesPanel)
		);
	
		container.setLayout(layout);
	}


	
	/**
	 * Set JButton menus itens
	 * 
	 * @return ArrayList<JButton>
	 */
	private ArrayList setMenu()
	{
		// Set menu itens
		menuItens.add("Reset");
		menuItens.add("Artistic Mosaic");
		menuItens.add("Artistic Television");
		menuItens.add("Skin color Detect");
		menuItens.add("ThreSholding");
		menuItens.add("Convolution");
		menuItens.add("Corner Detection");
		
		
		// Add menu itens in buttons array
		for (int i = 0; i < menuItens.size(); i++) {
			menu.add(new JButton(menuItens.get(i)));
		}
		
		return menu;
	}
	
	
	
	
	private void setImagePanel()
	{
		
	}
	
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
				
		// loop for check what button get clicked
		for (int i = 0; i < menu.size(); i++) {
			if ( e.getSource() == menu.get(i) ) {
				imageBkp = image.clone();
				
				attr = new MarvinAttributes();
				
				mask = new MarvinImageMask(100,100);
				System.out.println(mask.getMaskArray());
				
				if ( menu.get(i).getText() == "Reset" ) {
					imagePanel.setImage(image);
				}
				
				if ( menu.get(i).getText() == "Artistic Mosaic" ) {
					marvin.applyFilter(imagePanel, imageBkp, "org.marvinproject.image.artistic.mosaic.jar");
				}
				
				if ( menu.get(i).getText() == "Artistic Television" ) {
					marvin.resetImage(imagePanel);
					marvin.applyFilter(imagePanel, imageBkp, "org.marvinproject.image.artistic.television.jar");
				}
				
				if ( menu.get(i).getText() == "Skin color Detect" ) {
					marvin.resetImage(imagePanel);
					marvin.applyFilter(imagePanel, imageBkp, "org.marvinproject.image.color.skinColorDetection.jar");
				}
				
				if ( menu.get(i).getText() == "ThreSholding" ) {
					marvin.resetImage(imagePanel);
					marvin.applyFilter(imagePanel, imageBkp, "org.marvinproject.image.color.thresholding.jar");
				}
				
				if ( menu.get(i).getText() == "Convolution" ) {
					marvin.resetImage(imagePanel);
					marvin.applyFilter(imagePanel, imageBkp, "org.marvinproject.image.convolution.jar");
				}
				
				if ( menu.get(i).getText() == "Corner Detection" ) {
					marvin.resetImage(imagePanel);
					marvin.applyFilter(imagePanel, imageBkp, "org.marvinproject.image.corner.moravec.jar", attr, mask);
					
					
				}
				
				System.out.println(menu.get(i).getText());
				break;
			}
		}
		
		
	}
}
